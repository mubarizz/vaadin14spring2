package org.vaadin.v14s2.ui;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.HighlightConditions;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.PWA;
import org.vaadin.v14s2.ui.views.dashboard.DashboardView;
import org.vaadin.v14s2.ui.views.list.ListView;

@CssImport("./styles/shared-styles.css")
@PWA(
        name = "Vaadin14Demo",
        shortName = "V14",
        offlineResources = {
                "./styles/offline.css",
                "./images/offline.png"
        })
public class MainLayout extends AppLayout {

    public MainLayout() {
        createHeader();
        creatDrawer();
    }

    private void creatDrawer() {
        RouterLink listLink = new RouterLink("List", ListView.class);
        listLink.setHighlightCondition(HighlightConditions.sameLocation());

        addToDrawer(new VerticalLayout(
                listLink,
                new RouterLink("Dashboard", DashboardView.class)
        ));
    }

    private void createHeader() {
        H1 logo = new H1("Vaadin 14");
        logo.addClassName("logo");

        Anchor logout = new Anchor("/logout", "Log out");

        HorizontalLayout header = new HorizontalLayout(new DrawerToggle(), logo, logout);
        header.addClassName("header");
        header.setWidth("100%");
        header.expand(logo);
        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);

        addToNavbar(header);
    }


}
