package org.vaadin.v14s2.backend.entity;

//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;

//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class UserAccount extends AbstractEntity implements Serializable {

    /**
     *
     */

    @Serial
    private static final long serialVersionUID = 1L;
    private Title title;
    private String password;
    private String username;
    private String identifier;
    private String firstName;
    private String lastName;
    private String clinicalID;

    //for not to delete data
//    private String isactive;

//    private Boolean locked;
//    private Boolean enabled;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "profile")
    private Profile profile;

//    private boolean viewerNeeded;

//    @OneToMany(fetch = FetchType.LAZY)
//    private List<SimpleGrantedAuthority> grantedAuthorities;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_account_imaging_center", joinColumns = {
            @JoinColumn(name = "user_account_id") }, inverseJoinColumns = { @JoinColumn(name = "imaging_center_id") })
    //@Fetch(value = FetchMode.SUBSELECT)
    private Set<ImagingCenter> imagingCenters = new HashSet<ImagingCenter>();

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

//    public List<SimpleGrantedAuthority> getGrantedAuthorities() {
//        return grantedAuthorities;
//    }
//
//    public void setGrantedAuthorities(List<SimpleGrantedAuthority> grantedAuthorities) {
//        this.grantedAuthorities = grantedAuthorities;
//    }
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return null;
////        return grantedAuthorities;
//    }
//
//    @Override
//    public boolean isAccountNonExpired() {
//        return false;
//    }
//
//    @Override
//    public boolean isAccountNonLocked() {
//        return false;
//    }
//
//    @Override
//    public boolean isCredentialsNonExpired() {
//        return false;
//    }
//
//    @Override
//    public boolean isEnabled() {
//        return true;
//    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getClinicalID() {
        return clinicalID;
    }

    public void setClinicalID(String clinicalID) {
        this.clinicalID = clinicalID;
    }

    public String getFullName() {
        String full = "";

        if (title != null) {
            full = title + " " + firstName + " " + lastName;
        } else {
            // used rarely for the the user "all"
            full = firstName;
        }

        return full;
    }

    public String getName() {

        if (lastName == null) {
            return firstName;
        } else {
            return lastName + ", " + firstName;
        }

    }

    public Set<ImagingCenter> getImagingCenters() {
        return imagingCenters;
    }

    public void setImagingCenters(Set<ImagingCenter> imagingCenters) {
        this.imagingCenters = imagingCenters;
    }

    public String getFullNameWOC() {
        return firstName + " " + lastName;
    }

//    public boolean isViewerNeeded() {
//        return viewerNeeded;
//    }

//    public void setViewerNeeded(boolean viewerNeeded) {
//        this.viewerNeeded = viewerNeeded;
//    }
}
