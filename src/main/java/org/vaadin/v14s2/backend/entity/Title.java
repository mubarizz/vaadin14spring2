package org.vaadin.v14s2.backend.entity;

public enum Title {
    Mr, Ms, Mrs, Dr;
}
