package org.vaadin.v14s2.backend.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "profiles")
public class Profile extends AbstractEntity implements Serializable {

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private int id;
    @CreationTimestamp
    private Date date;
    @Column(name = "updated")
    @UpdateTimestamp
    private Date update;

    @Serial
    private static final long serialVersionUID = 1L;

    private String value;
    private String description;

    // Patient
    private boolean allPatient;
    private boolean patientCRUDAuthorized;
    private boolean patientDeleteAuthorized;
    private boolean patientSearchAuthorized;
    private boolean medicalDetailsAuthorized;
    private boolean examAuthorized;
    private boolean billingAuthorized;
    private boolean documentsAuthorized;
    private boolean notesAuthorized;
    private boolean importPatientAuthorized;
    private boolean editPatientCustomizedFieldsAuthorized;

    // Schedule
    private boolean allAppointments;
    private boolean appointmentCRUDAuthorized;
    private boolean appointmentDeleteAuthorized;
    private boolean appointmentSearchAuthorized;
    private boolean editOrderCustomizedFieldsAuthorized;
    private boolean editProcedureCustomizedFieldsAuthorized;

    // Calendar
    private boolean calendarAuthorized;
    private boolean appointmentInCalendarCRUDAuthorized;

    // Workflow
    private boolean allWorkflowAuthorized;
    private boolean filterCRUDAuthorized;
    private boolean manualSearchAuthorized;

    // Administration
    private boolean allAdministrationAuthorized;
    private boolean systemAuthorized;
    private boolean usersAuthorized;
    private boolean resourcesAuthorized;
    private boolean networkAuthorized;

    // Search
    private boolean allSearchAuthorized;
    private boolean exportSearchAuthorized;
    private boolean searchProfileAddAuthorized;
    private boolean searchProfileEditAuthorized;
    private boolean searchProfileDeleteAuthorized;
    private boolean searchProfileSaveInDBAuthorized;
    private boolean searchProfileSaveInSessionAuthorized;
    private boolean editResultAuthorized;
}
